# frozen_string_literal: true

class HomeController < ApplicationController
  def index; end

  def some_static_data
    data = {
      "userId": 1,
      "id": 1,
      "title": 'It works',
      "body": 'My Angular application is talking to my Rails Application'
    }
    sleep 2
    render json: data
  end

  def some_static_data
    data = {"userId": 1,
            "id": 1,
            "title": "It works",
            "body": "My Angular application is talking to my Rails Application"
    }
    render json: data
  end
end
