import {Component} from '@angular/core';

@Component({
  selector: 'demo-directives',
  template: `
            <h1>Directives demos</h1>
            <div *ngIf="false">Shoult not appear because of false</div>
            <div *ngIf="a > b">a is greather than b</div>
            <div *ngIf="str == 'yes'">str is yes</div>
            <div *ngIf="myFunction()"> my function is true</div>
            
            <div>Switch case
                <div class="container" [ngSwitch]="myVariable">
                    <div *ngSwitchCase="'A'">Var is A</div>
                    <div *ngSwitchCase="'B'">Var is B</div>
                    <div *ngSwitchDefault>Var is something else</div>
                </div>
            
                <div class="ui horizontal divider header">
                    Current choise is {{ choise }}
                </div>
            
                <div class="ui raised segment">
                    <ul [ngSwitch]="choise">
                        <li *ngSwitchCase="1">First choise</li>
                        <li *ngSwitchCase="2">Second choise</li>
                        <li *ngSwitchCase="3">Third choise</li>
                        <li *ngSwitchCase="4">Fourth choise</li>
                        <li *ngSwitchCase="2">Second choise again</li>
                        <li *ngSwitchDefault>Default choise</li>
                    </ul>
                </div>
            
                <div style="margin-top: 20px;">
                    <button class="ui primary button" (click)="nextChoice()">
                        Next choise
                    </button>
                </div>
            </div>
            
            <div>
                <div class="ui horizontal divider header">Styling ngStyle</div>
                <div [style.background-color]="'yellow'">
                    Uses fixed yellow backgroud
                </div>
                <div [ngStyle]="{color: 'white', 'background-color': 'blue'}">
                    Uses fixed white text on blue background
                </div>
                <div class="ui input">
                    <input type="text" name="color" value="{{color}}" #colorInput>
                </div>
                <div class="ui input">
                    <input type="text" name="fontSize" value="{{fontSize}}" #fontInput>
                </div>
                <button class="ui primari button" (click)="applySettings(colorInput.value, fontInput.value)">
                    Apply Settings
                </button>
                <div>
                    <span [style.color]="color" [style.fontSize.px]="fontSize">
                    {{color}} text
                    </span>
                </div>
            </div>
            
            <div class="container">
                <div class="ui horizontal divider header">ngClass directive</div>
                <div [ngClass]="{bordered: false}">This is never bordered</div>
                <div [ngClass]="{bordered: true}">This is always bordered</div>
                
                <div [ngClass]="{bordered: isBordered}">
                    Using object literal. Border {{ isBordered ? "ON" : "OFF" }}
                </div>
                
                <div [ngClass]="classObj">
                    Using object var. Border {{ classesObj.bordered ? "ON" : "OFF" }}
                </div>
                
                <div class="base" [ngClass]="['blue', 'round']">
                    This will always have a blue background and
                    round corners
                </div>
            </div>
            <div>
                <div class="ui horizontal divider header" [ngStyle]="{'margin-top': '20px'}">ngFor directive</div>
                <h4 class="ui divider horizontal header">Simple list of Strings</h4>
                <div class="ui list" *ngFor="let c of cities; let num = index">
                    <div class="item">{{ num + 1 }} - {{ c }}</div>
                </div>
                <h4 class="ui horizontal divider header">Simple list of objects</h4>
                <table class="ui celled table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Age</th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tr *ngFor="let p of people">
                        <td>{{ p.name }}</td>
                        <td>{{ p.age }}</td>
                        <td>{{ p.city }}</td>
                    </tr>
                </table>
            </div>
            <div>
                <div class="ui horizontal divider header" [ngStyle]="{'margin-top': '20px'}">ngNonBindable</div>
                <div class="ngNonBindableDemo">
                    <span class="pre" ngNonBindable>
                        This is what {{ str }} render &rarr; 
                    </span>
                    <span class="bordered">{{ str }}</span>
                </div>
            </div>
            `
})
export class AppComponent {
    cities: string[];
    people: Object[];
    a: number = 5;
    b: number = 3;
    isBordered: boolean;
    classesObj: Object;
    classList: string[];
    color: string;
    fontSize: number;
    str: String = 'yes';

    myVariable: String = 'K';
    myFunction(){ return true}

    choise: number;
    constructor() {
        this.choise = 3;
        this.color = 'red';
        this.fontSize = 12;
        this.cities = ['New York', 'Madrid', 'Moscow'];
        this.people = [
            {name: 'Ichiro', age: 43, city: 'New York'},
            {name: 'Nadal', age: 33, city: 'Madrid'},
            {name: 'Putin', age: 66, city: 'Moscow'}
            ];
    }

    ngOnInit() {
        this.isBordered = true;
        this.classList = ['blue', 'round'];
        this.toggleBorder();
    }

    toggleBorder(): void {
        this.isBordered = !this.isBordered;
        this.classesObj = {
            border: this.isBordered
        };
    }

    applySettings(color, fontSize){
        this.color = color;
        this.fontSize = fontSize;
    }

    nextChoice() {
        this.choise += 1
        if (this.choise == 6) {
            this.choise = 1
        }
    }
}
