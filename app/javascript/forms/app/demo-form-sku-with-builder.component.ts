import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-demo-from-sku-with-builder',
    template: `
                <div class="ui raised segment">
                    <div class="ui info message">
                        The product name is: {{ productName }}
                    </div>
                    <h2 class="ui right floated header">Whit Builder: SKU</h2>
                    <form [formGroup]="myForm"
                            (ngSubmit)="onSubmit(myForm.value)"
                            class="ui form"
                            [class.error]="!sku.valid && sku.touched">
                            <div class="field" [class.error]="!sku.valid && sku.touched">
                                <label for="skuInput">SKU</label>
                                <input type="text"
                                        id="skuInput"
                                        placeholder="SKU"
                                        [formControl]="sku">
                                <div *ngIf="!sku.valid"
                                    class="ui error">SKU is not valid</div>
                                <div *ngIf="sku.hasError('required')"
                                    class="ui error">This field is mandatory</div>
                                <div *ngIf="sku.hasError('invalidSku')"
                                    class="ui error">SKU must begin with <span>123</span></div>
                            </div>
                            <div class="field">
                                <label for="productNameInput">Product Name</label>
                                <input type="text"
                                        id="productNameInput"
                                        placeholder="Product Name"
                                        [formControl]="myForm.controls.productName"
                                        [(ngModel)]="productName">
                            </div>
                            <div *ngIf="!myForm.valid"
                                class="ui error">* This form is still not valid</div>
                            <button type="submit" class="ui button">Submit</button>
                    </form>
                </div>
              `
})

export class DemoFormSkuWithBuilderComponent implements OnInit {
    myForm: FormGroup;
    sku: AbstractControl;
    productName: string;

    constructor(fb: FormBuilder) {
        this.myForm = fb.group({
            'sku': ['', Validators.compose([
                Validators.required,
                this.skuValidator])],
            'productName': ['', Validators.required]
        });

        this.sku = this.myForm.controls['sku'];

        this.sku.valueChanges.subscribe(
            (value: string) => {
               console.log('sku value changed to', value)
            });

        this.myForm.valueChanges.subscribe(
            (form: any) => {
                console.log('form value changed to', form)
            }
        );
    }

    ngOnInit() {
    }

    onSubmit(value: string): void {
        console.log('you submitted value: ', value)
    }

    skuValidator(control: FormControl): { [s: string]: boolean } {
        if (!control.value.match(/^123/)) {
            return {invalidSku: true};
        }
    }
}