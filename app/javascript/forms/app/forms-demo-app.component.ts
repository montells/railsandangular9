import {Component} from '@angular/core';
import { DemoFormSkuComponent } from './demo-form-sku.component';

@Component({
  selector: 'montells-forms',
  template: `
            <h1>Forms Chapter</h1>
            <app-demo-from-sku></app-demo-from-sku>
            <div class="ui clearing divider"></div>
            <app-demo-from-sku-with-builder></app-demo-from-sku-with-builder>
            `
})
export class FormsDemoApp {
    cities: string[];
    people: Object[];
    a: number = 5;
    b: number = 3;
    isBordered: boolean;
    classesObj: Object;
    classList: string[];
    color: string;
    fontSize: number;
    str: String = 'yes';

    myVariable: String = 'K';
    myFunction(){ return true}

    choise: number;
    constructor() {
        this.choise = 3;
        this.color = 'red';
        this.fontSize = 12;
        this.cities = ['New York', 'Madrid', 'Moscow'];
        this.people = [
            {name: 'Ichiro', age: 43, city: 'New York'},
            {name: 'Nadal', age: 33, city: 'Madrid'},
            {name: 'Putin', age: 66, city: 'Moscow'}
            ];
    }

    ngOnInit() {
        this.isBordered = true;
        this.classList = ['blue', 'round'];
        this.toggleBorder();
    }

    toggleBorder(): void {
        this.isBordered = !this.isBordered;
        this.classesObj = {
            border: this.isBordered
        };
    }

    applySettings(color, fontSize){
        this.color = color;
        this.fontSize = fontSize;
    }

    nextChoice() {
        this.choise += 1
        if (this.choise == 6) {
            this.choise = 1
        }
    }
}
