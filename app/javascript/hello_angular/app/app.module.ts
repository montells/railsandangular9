import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world.component';
import { UserItemComponent } from './user-item.component';
import { UserListComponent} from './user-list.component';

@NgModule({
  declarations: [
    AppComponent, HelloWorldComponent, UserItemComponent, UserListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent, HelloWorldComponent, UserListComponent]
})
export class AppModule { }
