import { Component, Input } from '@angular/core';

@Component({
    selector: 'user-item',
    template: `<h1>{{ name }}</h1>`
})

export class UserItemComponent {
    @Input() name: string

    constructor() {
    }
}