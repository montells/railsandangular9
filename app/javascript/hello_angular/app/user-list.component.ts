import { Component } from '@angular/core';

@Component({
    selector: 'user-list',
    template: `<ul>
                <li *ngFor="let name of names">
                <user-item [name]="name"></user-item>
</li>
                </ul>`
})

export class UserListComponent {

    names: string[]

    constructor() {
        this.names = ['Felipe', 'Carlos', 'Juan', 'Ernaldo'];
    }
}