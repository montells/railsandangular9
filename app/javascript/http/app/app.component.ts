import { Component } from '@angular/core'

@Component({
    selector: 'http-app-root',
    template:   `
                    <div>
                        <simple-http-component></simple-http-component>
                        <div class="ui horizontal divider">Search YouTube</div>
                        <you-tube-search></you-tube-search>
                    </div>
                `
})
export class AppComponent{}