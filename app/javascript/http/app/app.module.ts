import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from './app.component';
import { SimpleHttpComponent } from './simple-http.component';
import { YouTubeSearchComponent } from './you-tube-search.component';
import { youtubeSearchInjectables } from './you-tube-search/you-tube-search.injectables'

@NgModule({
    declarations: [
        AppComponent,
        SimpleHttpComponent,
        YouTubeSearchComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [
        youtubeSearchInjectables
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}