import { Component, OnInit } from '@angular/core'
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'simple-http-component',
    template:   `
                <div>
                    <h2 class="ui header">
                        <i class="exchange icon"></i>
                        <div class="content">HTTP service
                            <div class="sub header">Basic request</div>
                        </div>
                    </h2>
                    <button class="ui primary button"
                            type="button"
                            (click)="makeRequest()">
                            Make Request
                    </button>
                    <div *ngIf="loading">loading...</div>                            
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <pre>
                                {{ data | json }}
                            </pre>
                        </div>
                    </div>
                </div>
                `
})
export class SimpleHttpComponent implements OnInit {

    loading: boolean = false;
    data: Object;

    ngOnInit() {}

    constructor(private http: HttpClient) {
        this.http = http;
        this.loading = false;
        this.data = 'Click button for getting data....';
    }

    makeRequest() {
        this.loading = true;
        this.data = undefined;
        this.http.get('http://localhost:3000/some_static_data.json')
            .subscribe(data => {
                this.data = data;
                this.loading = false;
            });
    }
}