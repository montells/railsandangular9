import { Component } from '@angular/core'

@Component({
    selector: 'you-tube-search',
    template:   `
                <h2 class="ui header">
                    <i class="youtube icon"></i>
                    <div class="content">Search on Youtube
                        <div class="sub header">Searching YouTube example</div>
                    </div>
                </h2>
                `
})
export class YouTubeSearchComponent{}