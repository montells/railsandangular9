import { Component } from '@angular/core';
import { Article } from './article/article.model';

@Component({
    template: ` <h1>App works</h1>
                <form class="ui large form segment">
                    <h3 class="ui header">Add a link</h3>
                    
                    <div class="field">
                        <label for="title">Title:</label>
                        <input name="title" #newTitle>
                    </div>
                    <div class="field">
                        <label for="link">Link:</label>
                        <input name="link" #newLink>
                    </div>
                    <button (click)="addArticle(newTitle, newLink)" 
                            class="ui positive right floated button">
                    Submit Link
                    </button>
                </form>
                <div class="ui grid posts">
                    <app-article 
                        *ngFor="let art of sortedArticles()"
                        [article]="art">
                    </app-article>
                </div>
                `,
    selector: 'app-root',
})

export class AppComponent {
    articles: Article[];

    constructor() {
        this.articles = [
            new Article('Angular 2', 'http://angular.io', 3),
            new Article('Rspec book', 'http://rspec.io', 2),
            new Article('Ruby on Rails', 'http://railsguide.com', 6),
        ];
    }

    addArticle(title: HTMLInputElement, link: HTMLInputElement): boolean {
        console.log(`Adding article title ${title.value} with link ${link.value}`);
        this.articles.push( new Article(title.value, link.value, 0) );
        title.value = '';
        link.value = '';
        return false;
    }

    sortedArticles(): Article[] {
        return this.articles.sort((a: Article, b: Article) => b.votes - a.votes)
    }
}