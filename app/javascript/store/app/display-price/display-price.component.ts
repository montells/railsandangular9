import {Component, Input} from '@angular/core';

@Component({
    selector: 'display-price',
    template: `
                <div class="display-price">\${{ price }}</div>
              `
})

export class DisplayPriceComponent{
    @Input() price: number
}