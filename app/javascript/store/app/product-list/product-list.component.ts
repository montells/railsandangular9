import {
    Component,
    EventEmitter,
    Input,
    Output
} from '@angular/core';
import { Product } from '../product.model';

@Component({
    selector: 'product-list',
    template: `
                <div class="ui items">
                    <product-row
                        *ngFor="let productItem of productList"
                        [product]="productItem"
                        (click)="clicked(productItem)"
                        [class.selected]="isSelected(productItem)">
                     </product-row>
                </div>
              `
})

export class ProductListComponent{

    @Input() productList: Product[];

    @Output() onProductSelected: EventEmitter<Product>;

    private currentProduct: Product;

    constructor(){
        this.onProductSelected = new EventEmitter()
    }

    clicked(product: Product): void {
        this.currentProduct = product;
        this.onProductSelected.emit(product);
    }

    isSelected(product: Product): boolean {
        if (!product || !this.currentProduct) {
            return false;
        }
        return product.sku === this.currentProduct.sku;
    }
}