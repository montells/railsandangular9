import {
    Component,
    Input,
    HostBinding
} from '@angular/core';
import { Product } from '../product.model';

@Component({
    selector: 'product-row',
    template: `
                <product-image [product]="product"></product-image>
                <div class="content">
                    <div class="header">{{ product.name }}</div>
                    <div class="meta">
                        <div class="product-sku">SKU #{{ product.sku }}</div>
                    </div>
                    <div class="description">
                        <product-department [product]="product"></product-department>
                    </div>
                </div>
                <display-price [price]="product.price"></display-price>
              `
})

export class ProductRowComponent{

    @Input() product: Product
    @HostBinding('attr.class') cssClass = 'item'

    constructor() {
    }
}