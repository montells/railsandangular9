Rails.application.routes.draw do
  root 'home#index'

  get 'some_static_data', to: 'home#some_static_data'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
